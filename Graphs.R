library(readxl)
library(ggplot2)

#pirmai
x <- read_excel("C://Users/ievtau/Documents/YX.xlsx")
y_predicted <- read_excel("C://Users/ievtau/Documents/y_predicted.xlsx")
validation <- read_excel("C://Users/ievtau/Documents/validation.xlsx")
predic <- cbind(y_predicted, validation)
as.data.frame(predic)
foo <- seq(1, 419328, by=1) 
foo <- as.vector(foo)
predict<-cbind(predic, foo)
test <- predict[c(1:75),] 
ggplot(test) + geom_line(aes(x=foo, y=validation, color="pcs")) + geom_line(aes(x=foo, y=y_predicted, col="y_predicted")) + scale_color_discrete(name="Legend")+ labs(title="predicted") 
library(reshape2)
df <- melt(test[, c("foo", "Y_predicted", "validation")], id="foo")
ggplot(df) + geom_line(aes(x=foo, y=value, color=variable)) + labs(title="regularization coefficient - 1e-5")# plot multiple time series by melting

#antrai
y <- read_excel("C://Users/ievtau/Documents/test.xlsx")
foo2 <- seq(1, 431543, by=1)
foo2 <- as.vector(foo2)
predict2<-cbind(y , foo2)
test2 <- predict2[c(1:75),] 

ggplot(y) + geom_line(aes(x=foo2, y=y_predicted, color="y_predicted")) + geom_line(aes(x=foo, y=Y_test, col="y_test")) + scale_color_discrete(name="Legend")+ labs(title="predicted") 
library(reshape2)
df <- melt(test2[, c("foo2", "Y_predicted", "Y_test")], id="foo2")
ggplot(df) + geom_line(aes(x=foo2, y=value, color=variable)) + labs(title="regularization coefficient - 0.01")# plot multiple time series by melting