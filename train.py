def mk_cmd_vw_pasted(path_predictions,path_data, path_combined):
    cmd = " ".join([
        "! paste -d",
        "' '",
        "{}".format(path_predictions) if path_predictions else "" ,
        "{}".format(path_data) if path_data else "" ,
        ">",
        "{}".format(path_combined) if path_combined else ""
    ])
    return cmd
def mk_cmd_vw_metrics(path_combined, path_metrics):
    cmd = " ".join([
        "! mpipe metrics-reg",
        "--has-r2",
        "{}".format(path_combined) if path_combined else "" ,
        "-o",
        "{}".format(path_metrics) if path_metrics else ""
    ])
    return cmd

import os
import subprocess


dir_output_base = "/home/vagrant/synced_dir/project_part1/data_raw/assignment_1/"
os.makedirs(dir_output_base, exist_ok=True)

dir_models = os.path.join(dir_output_base, "models")


L1s = [1e-4, 1e-5, 1e-6, 1e-7, 1e-8]

path_training = "/home/vagrant/synced_dir/project_part1/data_raw/assignment_1/train.vw"
path_validation = "/home/vagrant/synced_dir/project_part1/data_raw/assignment_1/validation.vw"

FILENAME_METRICS = "metrics.json"
FILENAME_VW_REGRESSOR = "regressor.vw_model"
FILENAME_READABLE_MODEL = "readable_model.txt"
FILENAME_PREDICTIONS = "y_predicted.txt"
FILENAME_Inverted_hash = "hash.invert_hash"
FILENAME_combined = "combined.txt"


for L1 in L1s:
    
    cmd_paste =mk_cmd_vw_pasted(
        path_predictions=os.path.join(
            dir_models, 
            dir_model, 
            FILENAME_PREDICTIONS)
        ,path_data= path_validation
        , path_combined=os.path.join(
            dir_models, 
            dir_model, 
            FILENAME_combined))
    subprocess.run(cmd_paste, shell = True)
    cmd_metrics = mk_cmd_vw_metrics(
    path_combined =os.path.join(
            dir_models, 
            dir_model, 
            FILENAME_combined),
    path_metrics =os.path.join(
            dir_models, 
            dir_model, 
            FILENAME_METRICS))
    subprocess.run(cmd_metrics, shell = True)
    